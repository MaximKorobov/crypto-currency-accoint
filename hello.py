#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import time

import ccxt
import sys
from flask import Flask, jsonify

from main.config.config import Config
from main.exchange.exchanges import Exchanges
from main.tool.resource import ResourceLocator, ResourceType

config_file = Config(ResourceLocator.get_resource(ResourceType.ConfigFile))
exchanges = Exchanges(config_file)

app = Flask(__name__)


def get_balance(exmo):
    try:
        exmo_balance = exmo.fetch_balance()

        total = dict()
        total['name'] = exmo.name
        if 'info' in exmo_balance and 'balances' in exmo_balance['info']:
            total['balances'] = exmo_balance['info']['balances']
        else:
            total['balances'] = exmo_balance['total']

        return total

    except ccxt.DDoSProtection as e:
        print(type(e).__name__, e.args, 'DDoS Protection (ignoring)')
    except ccxt.RequestTimeout as e:
        print(type(e).__name__, e.args, 'Request Timeout (ignoring)')
    except ccxt.ExchangeNotAvailable as e:
        print(type(e).__name__, e.args, 'Exchange Not Available due to downtime or maintenance (ignoring)')
    except ccxt.AuthenticationError as e:
        print(type(e).__name__, e.args, 'Authentication Error (missing API keys, ignoring)')


@app.route("/balance")
def hello():
    start = time.time()

    all_exchanges = exchanges.get_exchanges()

    json = jsonify([get_balance(exchange.get_exchange()) for exchange in all_exchanges])
    print(time.time() - start)

    return json


if __name__ == "__main__":
    app.run(host='0.0.0.0')
