#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import os
from enum import Enum


class ResourceType(Enum):
    ConfigFile, OutFolder = range(2)


class ResourceLocator:
    @staticmethod
    def get_resource(resource_type):
        file_folder = os.path.dirname(__file__)

        out_folder = file_folder + "/../out/"
        config_folder = file_folder + "/../config/"

        path = ""
        if resource_type == ResourceType.ConfigFile:
            path = os.path.abspath(config_folder + "config.ini")
        elif resource_type == ResourceType.OutFolder:
            path = os.path.abspath(out_folder) + "/"
            if not os.path.exists(path):
                os.makedirs(path)

        return path
