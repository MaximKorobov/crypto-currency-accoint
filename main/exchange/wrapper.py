#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import ccxt


class ExchangeWrapper:

    def __init__(self, exchange, name):
        self.exchange = exchange
        self.name = name

    def get_exchange(self):
        return self.exchange

    def get_balance(self):
        total = dict()
        total['name'] = self.exchange.name
        try:
            exchange_balance = self.exchange.fetch_balance()
            # print(exmo.name, 'balance', exmo_balance)

            # total['balances'] = exmo_balance['info']['balances']

            if 'info' in exchange_balance and 'balances' in exchange_balance['info']:
                total['balances'] = exchange_balance['info']['balances']
            else:
                total['balances'] = exchange_balance['total']
            # total['uid'] = exmo_balance['info']['uid']
            # total['overall'] = {'curr': 'BTC', 'value':'0'}
            return total


            # EXMO
            # Manual: http://proxy-it.nordvpn.com/browse.php?u=bUgkF5aXl6eSFg2YR1Ie38bKV08%3D&b=1&pr=
            # exmo_balance = exmo.fetch_balance()
            # print('balance', exmo_balance)

            # total = exmo_balance['total']
            # print(json.dumps(total, indent=4, sort_keys=True))

            # 1504126800 = 31 августа
            # a = exmo.request('wallet_history', 'private', 'POST', params={'date': '1504126800'})

            # a = exmo.request('wallet_convert', 'private', 'POST', params={'currency': 'USD'})

            # a = exmo.request('user_info', 'private', 'POST')

            # print(a)

            # Bitlish
            # bitlish_balance = bitlish.fetch_balance()
            # print(bitlish_balance['total'])

            # Livecoin
            # exchange_balance = livecoin.fetch_balance()
            # print(exchange_balance['total'])

            # balance = self.exchange.fetch_balance()
            # print(exchange.name)
            # print(exchange_balance['total'])
            # return balance

        except ccxt.DDoSProtection as e:
            print(type(e).__name__, e.args, 'DDoS Protection (ignoring)')
        except ccxt.RequestTimeout as e:
            print(type(e).__name__, e.args, 'Request Timeout (ignoring)')
        except ccxt.ExchangeNotAvailable as e:
            print(type(e).__name__, e.args, 'Exchange Not Available due to downtime or maintenance (ignoring)')
        except ccxt.AuthenticationError as e:
            print(type(e).__name__, e.args, 'Authentication Error (missing API keys, ignoring)')
        finally:
            return total

    def get_history(self):
        return []


