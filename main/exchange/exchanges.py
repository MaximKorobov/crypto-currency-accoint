#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import ccxt

from main.exchange.wrapper import ExchangeWrapper


class Exchanges:
    @staticmethod
    def get_prepared_exchanges(config):
        result = []

        for section_name in config.sections():
            if section_name.find('Account') != -1:
                section = config.config_section_map(section_name)
                params = dict()
                exchange_class = None
                exchange_name = None
                for item in section.items():
                    # if item[1] in ccxt.exchanges:
                    if item[0] == 'name':
                        exchange_name = item[1]
                        exchange_class = getattr(ccxt, exchange_name)
                    else:
                        params[item[0]] = item[1]

                if exchange_class:
                    exchange = exchange_class(params)
                    # exchanges = ccxt.bitmex
                    result.append((exchange, exchange_name))

        return result

    def __init__(self, config):
        self.__exchanges__ = []

        exchanges = self.get_prepared_exchanges(config)
        for exchange in exchanges:
            self.__exchanges__.append(ExchangeWrapper(exchange[0], exchange[1]))

    def get_exchanges(self):
        return self.__exchanges__

    def get_exchange(self, name):
        for exchange in self.__exchanges__:
            if exchange.name.lower() == name.lower():
                return exchange

        return
