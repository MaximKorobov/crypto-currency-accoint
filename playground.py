#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import time
from multiprocessing.pool import ThreadPool

import datetime
from ccxt import livecoin

from main.config.config import Config
from main.exchange.exchanges import Exchanges
from main.tool.resource import ResourceLocator, ResourceType

start = time.time()

config_file = Config(ResourceLocator.get_resource(ResourceType.ConfigFile))
wrappers = Exchanges(config_file)
exchanges = wrappers.get_exchanges()

# Async
# exchange = exchanges.get_exchange("livecoin").get_exchange()
# all_exchanges = exchanges.exchanges
#
#
# def get_balance(exchange):
#     return exchange.get_balance()
#
#
# pool = ThreadPool(len(all_exchanges))
# balances = pool.map(get_balance, all_exchanges)
#
# pool.close()
# pool.join()
#
# for exchange_balance in balances:
#     print(exchange_balance)

# e = livecoin()
# e.hasFetchMyTrades
# e.fetch_tick('')

# My orders and trades
# def print_list(symbol, list):
#     if len(list) > 0:
#         print('\n\n')
#         print(symbol)
#         print('\n')
#         print(list)
#
#
# for exchange in map(lambda e: e.exchange, exchanges):
#     if exchange.hasFetchMyTrades or exchange.hasFetchOrders:
#         exchange.load_markets()
#         for symbol in exchange.markets:
#             trades = exchange.fetchMyTrades(symbol)
#             print_list(symbol, trades)
#
#             orders = exchange.fetchMyTrades(symbol)
#             print_list(symbol, orders)


# exchange = wrappers.get_exchange("exmo").get_exchange()
# info = exchange.privatePostWalletHistory()
exchange = wrappers.get_exchange("livecoin").get_exchange()
unix_now = time.mktime(datetime.datetime.now().timetuple())
# info = exchange.privateGetPaymentHistorySize({'start': '1500020636000', 'end': str(int(unix_now)) + '000'})
info = exchange.privateGetPaymentHistoryTransactions({'start': '1500020636000', 'end': '1520020636000'})
print(info)


end = time.time()
print("%.2f sec" % (end - start))
