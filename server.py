#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import datetime
import time
from multiprocessing.pool import ThreadPool

import ccxt
from flask import Flask, jsonify
from flask_restplus import Api, Resource

from main.config.config import Config
from main.exchange.exchanges import Exchanges
from main.tool.resource import ResourceLocator, ResourceType

config_file = Config(ResourceLocator.get_resource(ResourceType.ConfigFile))
exchanges = Exchanges(config_file)

app = Flask(__name__)
app.url_map.strict_slashes = False

version = '1.0'
api = Api(app, version=version, title='Crypto account API',
          description='A simple API to manipulate crypto account on crypto exchanges')

exchange_ns = api.namespace('exchange', description='exchanges')
account_ns = api.namespace('account', description='exchange accounts')
operations_ns = api.namespace('operations', description='account operations')


@exchange_ns.route("/exchange/")
@exchange_ns.route("/exchange/list/<exchange_name>")
class Exchanges(Resource):

    def get(self, exchange_name=None):
        """List available exchanges"""
        exchanges_meta = {}

        for exchange_id in ccxt.exchanges:
            exchange = getattr(ccxt, exchange_id)
            exchanges_meta[exchange_id] = exchange()

        result = []
        tuples = list(ccxt.Exchange.keysort(exchanges_meta).items())
        for (exchange_id, params) in tuples:
            exchange = exchanges_meta[exchange_id]
            website = exchange.urls['www'][0] if type(exchange.urls['www']) is list else exchange.urls['www']
            logo = exchange.urls['logo']

            if exchange_name is None or exchange_name.lower() in exchange.name.lower():
                result.append({
                    "id": exchange.id, "name": exchange.name,  "website": website,
                    "requiredCredentials": exchange.requiredCredentials, "logo": logo
                })

        json_data = jsonify({"exchanges": result, "count": len(result)})
        return json_data


@account_ns.route("/<exchange_name>")
class Exchange(Resource):

    def post(self):
        """Attach exchange"""
        return None

    def delete(self):
        """Deattach exchange"""
        return None


@account_ns.route("/info")
@account_ns.route("/info/<exchange_name>")
class AccountInfo(Resource):

    def get(self, exchange_name=None):
        """List exchange accounts"""
        start = time.time()

        if exchange_name is not None:
            balances = []
            exchange_wrapper = exchanges.get_exchange(exchange_name)
            if exchange_wrapper:
                exchange_balance = exchange_wrapper.get_balance()
                balances.append(exchange_balance)
        else:
            all_exchanges = exchanges.__exchanges__
            pool = ThreadPool(len(all_exchanges))
            balances = pool.map(lambda ex: ex.get_balance(), all_exchanges)

            pool.close()
            pool.join()

        json_data = jsonify(balances)

        end = time.time()
        print("%.2f sec" % (end - start))

        return json_data


parser = api.parser()
parser.add_argument('account', type=str, location='args', required=True, default="exmo")
parser.add_argument('transfers', type=bool, location='args', required=True, default=True)
parser.add_argument('trades', type=bool, location='args', required=True, default=True)
parser.add_argument('start', type=datetime.date, location='args', required=False)
parser.add_argument('end', type=datetime.date, location='args', required=False)


@operations_ns.expect(parser)
@operations_ns.route("/operations/list/")
class Operations(Resource):

    def get(self):
        args = parser.parse_args()
        exchange_name = args['account']

        exchange_wrapper = exchanges.get_exchange(exchange_name)
        if exchange_wrapper:

            return ""

        return ""


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5001)
